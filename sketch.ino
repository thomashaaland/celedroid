#include <Stepper.h>
#include <LiquidCrystal_I2C.h>
#include <LedControl.h>
#include "pitches.h"

// StepMotors
#define stepOneDir 12
#define stepOneSTEP 13
#define stepTwoDir 3
#define stepTwoSTEP 4
#define stepThreeDir 5
#define stepThreeSTEP 6

// LDR Characteristics
const float GAMMA = 0.7;
const float RL10 = 50;

LiquidCrystal_I2C lcd(0x27, 20, 4);

#define LDR_PIN 2

// Joystick pins
#define HORZ A0
#define VERT A1
#define SEL 7

// Speaker pin
#define SPEAKER_PIN 11

// LED Matrix
#define CLK 10
#define DIN 8
#define CS  9
#define X_SEGMENTS 3
#define Y_SEGMENTS 1
#define NUM_SEGMENTS (X_SEGMENTS * Y_SEGMENTS)

// Class for managing LED matrices
LedControl mat = LedControl(DIN, CLK, CS, Y_SEGMENTS); // MAX7219

class DrinkServer
{
  public:
    DrinkServer()
    {
    }
    void drinkUpdater()
    {
      if (EP < 60000)
      {
        currentTime = millis();
        EP = currentTime - startTime;
      }
    }
    void serveDrinks(int coffeeAmount, int chocolateAmount, int milkAmount)
    {
      if (EP < (10 * coffeeAmount))
      {
        addCoffee();
        coffeeReset = 50;
      }
      else
        stopCoffee();
      if (EP > (coffeeAmount * 10) && EP < (coffeeAmount * 10) + (chocolateAmount * 10))
      {
        addChocolate();
        chocolateReset = 50;
      }
      else
        stopChocolate();
      if (EP > ((coffeeAmount * 10) + (chocolateAmount * 10)) && EP < ((coffeeAmount * 10) + (chocolateAmount * 10) + (milkAmount * 10)))
      {
        addMilk();
        milkReset = -50;
      }
      else
        stopMilk();
    }
    void addCoffee()
    {
      if (coffeeSteps > 0)
      {
        digitalWrite(stepOneDir, LOW);
        digitalWrite(stepOneSTEP, HIGH);
        digitalWrite(stepOneSTEP, LOW);
        coffeeSteps--;
      }
    }
    void addChocolate()
    {
      if (chocolateSteps > 0)
      {
        digitalWrite(stepTwoDir, LOW);
        digitalWrite(stepTwoSTEP, HIGH);
        digitalWrite(stepTwoSTEP, LOW);
        chocolateSteps--;
      }
    }
    void addMilk()
    {
      if (milkSteps > 0)
      {
        digitalWrite(stepThreeDir, LOW);
        digitalWrite(stepThreeSTEP, HIGH);
        digitalWrite(stepThreeSTEP, LOW);
        milkSteps--;
      }
    }
    void stopCoffee()
    {
      if (coffeeReset > 0)
      {
        digitalWrite(stepOneDir, HIGH);
        digitalWrite(stepOneSTEP, HIGH);
        digitalWrite(stepOneSTEP, LOW);
        coffeeReset--;
      }
    }
    void stopChocolate()
    {
      if (chocolateReset > 0)
      {
        digitalWrite(stepTwoDir, HIGH);
        digitalWrite(stepTwoSTEP, HIGH);
        digitalWrite(stepTwoSTEP, LOW);
        chocolateReset--;
      }
    }
    void stopMilk()
    {
      if (milkReset > 0)
      {
        digitalWrite(stepThreeDir, HIGH);
        digitalWrite(stepThreeSTEP, HIGH);
        digitalWrite(stepThreeSTEP, LOW);
        milkReset--;
      }
    }
    void resetTimer()
    {
      startTime = millis();
      EP = 0;
      coffeeSteps = 50;
      chocolateSteps = 50;
      milkSteps = 50;
    }
    long coffeeSteps;
    long chocolateSteps;
    long milkSteps;
    long coffeeReset;
    long chocolateReset;
    long milkReset;
    long currentTime;
    long startTime;
    long EP;
} servingMachine;

class SongPlayer {
public:
  SongPlayer() {
    currentNoteIndex = 0;
    inPause = true;
    timeStart = millis();
    on = false;
  }

  void startNote(int idx) {
    tone(SPEAKER_PIN, tones[notesheet[idx]]);
    duration = noteTypes[idx];
    pauseAfterTone = duration;
  }

  void stopNote()
  {
    noTone(SPEAKER_PIN);
    if (pauseAfterTone) {
      duration = pause;
    } else {
      duration = 0;
    }
  }

  void playSong() {
    if (!on) return;
    timeDelta = millis() - timeStart;
    if (timeDelta < duration) return;
    timeStart = millis();
    if (inPause) {
      startNote(currentNoteIndex);
      currentNoteIndex++;
      if (currentNoteIndex == 63) {
        stopNote();
        currentNoteIndex = 0;
        duration = nextSong;
      } 
      inPause = !inPause;
    } else {
      stopNote();
      inPause = !inPause;
    }
  }

  uint8_t getNote() {
    return notesheet[currentNoteIndex];
  }

  void toggle() {
    if (on) {
      stopNote();
    }  
    on = !on;
    delay(100);
  }

private:
  int duration;
  int timeStart;
  int timeDelta;
  int currentNoteIndex;
  bool inPause;
  bool pauseAfterTone;
  bool on;

  // Setup Music
  const int tones[8] = {
    NOTE_D4, NOTE_E4, NOTE_F4, NOTE_G4,
    NOTE_A4, NOTE_B4, NOTE_C5, NOTE_D5
  };

  const int pause = 100;
  const int nextSong = 2000;
  const int n = 1000;
  const int hn = n/2;
  const int qn = n/4;
  const int qnd = (int)(qn*1.5);
  const int en = n/8;
  const int enp = n/8; // + pause;
  const int sn = n/16;
  enum notes {D, E, F, G, A, B, C, Dp};

  int notesheet[63] = { B, B, C, Dp, 
                        Dp, C, B, A, 
                        G, G, A, B, 
                        B, A, A, 
                        B, B, C, Dp,
                        Dp, C, B, A,
                        G, G, A, B,
                        A, G, G, 
                        A, A, B, G,
                        A, B, C, B, G,
                        A, B, C, B, A,
                        G, A, D,
                        B, B, C, Dp,
                        Dp, C, B, A,
                        G, G, A, B,
                        A, G, G, G};

  int noteTypes[63] = { qn, qn, qn, qn,
                        qn, qn, qn, qn,
                        qn, qn, qn, qn,
                        qnd, en, hn,
                        qn, qn, qn, qn,
                        qn, qn, qn, qn,
                        qn, qn, qn, qn,
                        qnd, en, hn,
                        qn, qn, qn, qn,
                        qn, enp, en, qn, qn,
                        qn, enp, en, qn, qn,
                        qn, qn, hn,
                        qn, qn, qn, qn,
                        qn, qn, qn, qn,
                        qn, qn, qn, qn,
                        qnd, en, hn, hn};
} musicPlayer;

// Capture Joystick functionality
class Joystick {
public:
  Joystick(int vert, int horz, int sel) : vert{vert}, horz{horz}, sel{sel} {
    pinMode(vert, INPUT);
    pinMode(horz, INPUT);
    pinMode(sel, INPUT_PULLUP);
    vertVal = 512;
    horzVal = 512;
    selPressed = false;
  }

  void recieve() {
    vertVal = analogRead(vert);
    horzVal = analogRead(horz);
    selPressed = digitalRead(sel) == LOW;
  }

  int getVertVal() {
    if (vertVal < 512/2) return -1;
    else if (vertVal > 512 + 512/2) return 1;
    else return 0;
  }

  int getHorzVal() {
    if (horzVal < 512/2) return -1;
    else if (horzVal > 512 + 512/2) return 1;
    else return 0;
  }

  bool getPressedVal() {
    return selPressed;
  }
private:
  int vertVal, horzVal, selPressed;
  int vert, horz, sel;
} joystick(VERT, HORZ, SEL);

// Names for input responses
enum btn_name { UP, DOWN, RIGHT, LEFT, SELECT };
// Listener class is responsible for interpreting signals from joystick
class Listener {
public:
  Listener() {  }

  btn_name joystick_listen() {
    int result;
    timeDelta = millis() - timeStart;
    if (timeDelta < pauseBetween & (previousResult != -1)) {
      return -1;
    } else {
      timeStart = millis();
      joystick.recieve();
      int vert = joystick.getVertVal();
      int horz = joystick.getHorzVal();
      int sel = joystick.getPressedVal();
      //Serial.println(vert);
      if (vert < 0) result = UP;
      else if (vert > 0) result = DOWN;
      else if (horz < 0) result = RIGHT;
      else if (horz > 0) result = LEFT;
      else if (sel) result = SELECT;
      else result = -1;
    }
    previousResult = result;
    return result;
  }
private:
  int pauseBetween = 250;
  int timeStart = millis();
  int timeDelta;
  int previousResult = -1;
} listener;

class LcdControl {
public:
  updateLCD(int new_selection) {
    lcd.setCursor(0, select);
    lcd.print(" ");
    lcd.setCursor(0, new_selection);
    lcd.print("*");
    select = new_selection;
  }

  void updateMenu();

private:
  int select = 1;
} lcdController;

class MachineController {
public:
  MachineController() {
    currentSelection = 1;
    mat.shutdown(0, false);
    mat.setIntensity(0, 7);
    mat.clearDisplay(0);
  }

  void setup() {
    // Initialize the LCD
    pinMode(LDR_PIN, OUTPUT);
    lcd.init();
    lcd.backlight();
    lcd.print("Celedroid! Menu:");
    lcdController.updateMenu();


    //lcd.setCursor(1, 1);
    //lcd.print("1. Stop/Play music");
    //lcd.setCursor(1,2);
    //lcd.print("2. Mix beverage");
    lcdController.updateLCD(1);

    // Setup music:
    pinMode(SPEAKER_PIN, OUTPUT);
  }

  void listen() {
    response = listener.joystick_listen();
    updateResponse();
  }

  void updateResponse() {
    if (response == -1) return;
    if (response == UP) {
      up();
    }
    if (response == DOWN) {
      down();
    }
    if (response == SELECT) {
      select();
    }
  }

  void up() {
    if (currentSelection < 3) {
      currentSelection++;
    }
  }

  void down() {

    if (currentSelection > 1) {
      currentSelection--;
    }
  }

  void select() {
    if (currentSelection == 1) {
      musicPlayer.toggle();
    } else if (currentSelection == 2) {
      servingMachine.resetTimer();
    }
  }

  void updateMusic() {
    musicPlayer.playSong();
  }

  void updateLCD() {
    if (response == -1) return;
    lcdController.updateLCD(currentSelection);
  }

  void updateLEDMatrix() {
    timeDelta = millis() - timeStart;
    if (timeDelta > ledUpdateFreq) {
      timeStart = millis();
      for (uint8_t i = 0; i < 8; i++) {
        mat.setRow(0, i, signal[i]);
        signal[i] = signal[i] << 1;
      }
      int note = musicPlayer.getNote();
      signal[note] = signal[note] | 3;
    }
  }

  String getMenuElement(int i) {
    if (i < 0) return "";
    if (i > 3) return "";
    return menuSelection[i];
  }
private:
  int currentSelection;
  int response;
  byte signal[8] = {0, 0, 0, 0, 0, 0, 0, 0};
  int ledUpdateFreq = 200;
  int timeStart = millis();
  int timeDelta;
  String menuHeader = "Celedroid! Menu:";
  String menuSelection[4] = { "1. Stop/Play music", 
                              //"2. Pause/Unpause",
                              "2. Mix beverage",
                              "3. Set amount"
                              };
} centralControl;

void LcdControl::updateMenu() {
  int scroll = (select == 1) ? 0 : ((select == 4) ? 2 : (select - 1));
  for (size_t i = 0; i < 3; i++) {
    lcd.setCursor(1, i - scroll + 1);
    lcd.print(centralControl.getMenuElement(i));
  }
}

void setup() {
  // Testing to see if serving works
  servingMachine.EP = 70000;
  /**
  servingMachine.addCoffee(100);
  delay(1000);
  servingMachine.addMilk(100);
  delay(1000);
  servingMachine.addChocolate(100);
  delay(1000);
  **/
  centralControl.setup();
}

void loop() {
  // put your main code here, to run repeatedly:
  centralControl.listen();
  centralControl.updateMusic();
  centralControl.updateLCD();
  centralControl.updateLEDMatrix();
  servingMachine.drinkUpdater();
  servingMachine.serveDrinks(500, 500, 500);
}

# Celedroid

A party droid (actually a robot) schematics for Arduino Nano made in [wokwi](wokwi.com).

## Intro

This robot serves drinks, blinks with party lights and plays music for the entertainment of your guests, bringing a whole disco parlour right home into your livingroom.

## Contents

* [What is this?](#what-is-this)
* [Build](#build)
* [Usage](#usage)
* [Contribute](#contribute)

## What is this?

This project is schematics and code for making a party droid from components for Arduino Nano.

### Build

To build this project you need 
* 1 Arduino Nano board
* 3 Step motors
* 3 LED matrices
* 1 Joystick
* 1 Buzz speaker
* 1 LCD screen

### Usage

Assemble the parts according to the [schematics](#celedroid).

## Contribute

Contributors are Paal Marius Haugen and Thomas Haaland.

![Celedroid](celedroid.png)
